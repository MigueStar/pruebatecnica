//
//  GetPokemosDTO.swift
//  PruebaTecnica
//
//  Created by Miguel Fernandez on 25/11/21.
//

import Foundation

struct GetPokemosDTO: Codable {
    
    var count: Int
    var next: String
    var previous: String?
    var results: [Pokemon]
    
}
