//
//  PokeService.swift
//  PruebaTecnica
//
//  Created by Miguel Fernandez on 25/11/21.
//
import Alamofire
import Foundation

enum Response<T> {
    case success(T)
    case failure(Error)
}
//https://pokeapi.co/api/v2/pokemon?limit=50
struct PokeService {
    
    private let baseUrl = "https://pokeapi.co/api/v2/"
    
    
    func getPokemons(limit: Int = 50, completion: @escaping (Response<[Pokemon]>) -> Void) {
        let url = "\(baseUrl)pokemon?limit=\(limit)"
        let request = AF.request(url, method: .get)
        request.validate().responseDecodable(of: GetPokemosDTO.self) { response in
            switch response.result {
            case .success(let dto):
               let pokemons = dto.results
               completion(.success(pokemons))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getPoke(by name: String = "bulbasaur", completion: @escaping (Response<Pokemon>) -> Void) {
        completion(.success(Pokemon(dummy: true)))
    }
    
}
