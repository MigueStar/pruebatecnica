//
//  Usuario.swift
//  PruebaTecnica
//
//  Created by Miguel Fernandez on 25/11/21.
//

import Foundation

struct Usuario: Identifiable {
    var id: UUID = UUID()
    var firstName: String
    var lastName: String
    var email: String
    var phone: String
    
    var fullName: String {
        "\(firstName) \(lastName)"
    }
    init(mock: Bool = false) {
        if mock {
            firstName = "Rolando"
            lastName = "Fernandez"
            email = "mfernandezc182@gmail.com"
            phone = "924753449"
        } else {
            firstName = ""
            lastName = ""
            email = ""
            phone = ""
        }
    }
}
