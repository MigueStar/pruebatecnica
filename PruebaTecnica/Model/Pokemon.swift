//
//  Pokemon.swift
//  PruebaTecnica
//
//  Created by Miguel Fernandez on 25/11/21.
//

import Foundation

struct Pokemon: Codable {
    
    enum CodingKeys: String, CodingKey {
        case nombre = "name"
        case link = "url"
    }
    
    var nombre: String = ""
    var link: String = ""
    
    init(dummy:Bool? = nil){
        guard let isDummy = dummy else { return }
        if isDummy {
            nombre = "Pikachu"
        }
    }
}
