//
//  PokeListViewModel.swift
//  PruebaTecnica
//
//  Created by Miguel Fernandez on 25/11/21.
//

import Foundation
import Combine


class PokeListViewModel: ObservableObject {
    
  @Published  var pokemons: [Pokemon] = []
  @Published var showError: Bool = false
    private(set) var error: Error? {
        didSet{
            showError = error != nil
        }
    }
   
    func getPoke() {
        let service = PokeService()
        service.getPokemons{ [weak self] response in
            switch response{
            case .success(let pokemons):
                self?.pokemons = pokemons
            
            case .failure(let error):
                self?.error = error
            }
            
        }
    }
}
