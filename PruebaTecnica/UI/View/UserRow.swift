//
//  UserRow.swift
//  PruebaTecnica
//
//  Created by Miguel Fernandez on 25/11/21.
//

import SwiftUI

struct UserRow: View {
    
    var user: Usuario
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            HStack {
                Image(systemName: "person.circle.fill")
                    .foregroundColor(.white)
                Text(user.fullName)
                    .font(.headline)
            }
            
            
            Text(user.email)
                .font(.body)
                .foregroundColor(Color(UIColor.darkGray))
        }
    }
}

struct UserRow_Previews: PreviewProvider {
    static var previews: some View {
        UserRow(user: .init(mock: true))
    }
}
