//
//  PruebaTecnicaApp.swift
//  PruebaTecnica
//
//  Created by Miguel Fernandez on 25/11/21.
//

import SwiftUI

@main
struct PruebaTecnicaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
