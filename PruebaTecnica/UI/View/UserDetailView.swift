//
//  UserDetailView.swift
//  PruebaTecnica
//
//  Created by Miguel Fernandez on 25/11/21.
//

import SwiftUI

struct UserDetailView: View {
    
    @Environment(\.presentationMode) private var presentationMode
    var user: Usuario
    
    var body: some View {
        Form {
            Section {
                HStack {
                    Text("Nombre:").bold()
                    Divider().padding(.trailing)
                    Text(user.firstName)
                }
                HStack {
                    Text("Apellido:").bold()
                    Divider().padding(.trailing)
                    Text(user.lastName)
                }
                HStack {
                    Text("Celular:").bold()
                    Divider().padding(.trailing)
                    Text(user.phone)
                }
                HStack {
                    Text("Email:").bold()
                    Divider().padding(.trailing)
                    Text(user.email)
                }
            }
            
            Section {
                Button("GO BACK") {
                    self.presentationMode.wrappedValue.dismiss()
                }
            }
        }
        .navigationTitle("Detalle Usuario")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct UserDetailView_Previews: PreviewProvider {
    static var previews: some View {
        UserDetailView(user: .init(mock: true))
    }
}
