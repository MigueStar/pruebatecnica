//
//  ContentView.swift
//  PruebaTecnica
//
//  Created by Miguel Fernandez on 25/11/21.
//

import SwiftUI

struct ContentView: View {
    
    @State var users: [Usuario] = []
    
    
    var body: some View {
        NavigationView {
            List {
                    Section(header:
                                UsersAddHeaderView(action: addUser(user:))
                            , footer:
                                Text("Cantidad: \(users.count)")
                    ) {
                        ForEach(users) { us in
                            NavigationLink(destination:
                                            UserDetailView(user: us)
                            ) {
                                UserRow(user: us)
                            }
                            .id(us.id)
                        }
                        .onDelete(perform: deleteUsers(from:))
                        .onMove(perform: moveUser(from:to:))
                    }
                    .listRowBackground(Color.green)
                }
            .listStyle(InsetGroupedListStyle())
            //.navigationTitle("Listado Alumnos")
            //.navigationBarTitleDisplayMode(.large)
            .navigationBarTitle(Text("Listado Usuarios"), displayMode: .inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                    }, label: {
                                        Image(systemName: "menubar.rectangle")
                                    })
                                ,
                                trailing:
                                    HStack {
                                        Button(action: {
                                            
                                        }, label: {
                                            Image(systemName: "plus")
                                        })
                                        EditButton()
                                    }
                                   
                                )
            
        }
        
    }
    func addUser(user: Usuario) {
        self.users.append(user)
    }
    
    func deleteUsers(from indexSet: IndexSet) {
        for index in indexSet {
            self.users.remove(at: index)
        }
    }
    
    func moveUser(from indexSet: IndexSet, to destiny: Int) {
        guard let index = indexSet.first else { return }
        users.insert(users.remove(at: index), at: destiny)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        
        ContentView(users: [.init(mock: true),
                               .init(mock: true),
                               .init(mock: true)])
    }
}
