//
//  UsersAddHeaderView.swift
//  PruebaTecnica
//
//  Created by Miguel Fernandez on 25/11/21.
//

import SwiftUI
import Combine

struct UsersAddHeaderView: View {
    
    @Environment(\.presentationMode) private var presentationMode
    
    @State var newUser: Usuario = Usuario()
    var full: Bool = false
    var action: (Usuario) -> Void
    let textLimit = 9
    
    var body: some View {
        ZStack {
            if presentationMode.wrappedValue.isPresented {
                Color.black.opacity(0.8)
                    .ignoresSafeArea()
            }
            VStack {
                TextField("Nombre", text: $newUser.firstName)
                    .keyboardType(.asciiCapable)
                    .textContentType(.name)
                    .disableAutocorrection(true)
                Divider()
                TextField("Apellido", text: $newUser.lastName)
                    .keyboardType(.asciiCapable)
                    .textContentType(.familyName)
                    .disableAutocorrection(true)
                Divider()
                TextField("Celular", text: $newUser.phone)
                    .onReceive(Just(newUser.phone), perform: { _ in
                        limitText(textLimit)
                    })
                    .keyboardType(.numberPad)
                    .disableAutocorrection(true)
                Divider()
                TextField("Email", text: $newUser.email)
                    .keyboardType(.emailAddress)
                    .textContentType(.emailAddress)
                    .disableAutocorrection(true)
                
                Button(action: {
                    self.action(newUser)
                    self.newUser = Usuario()
                    self.presentationMode.wrappedValue.dismiss()
                    
                }, label: {
                    Text("Agregar")
                        .foregroundColor(.white)
                        .padding()
                        .frame(maxWidth: .infinity)
                })
                .background(Color.blue)
                .cornerRadius(20)
                .padding(.vertical)
            }
            .padding(full == true ? .all : .top)
            .background(full == true ? Color.white : Color.clear)
            .padding(full == true ? .all : .bottom)
        }

    }
    func limitText(_ upper: Int)  {
        if newUser.phone.count > upper {
            newUser.phone = String(newUser.phone.prefix(upper))
        }
    }
}

struct UsersAddHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        
        Group {
            UsersAddHeaderView(full: true){ _ in
                
            }
            UsersAddHeaderView { _ in
                
            }
        }
        
        
    }
}
