//
//  UserListView.swift
//  PruebaTecnica
//
//  Created by Miguel Fernandez on 25/11/21.
//

import SwiftUI

struct UserListView: View {
    
    @State var users: [Usuario] = []
    @State var showAddUser = false
    
    var body: some View {
        NavigationView{
            List {
                    Section(footer: Text("Cantidad: \(users.count)")) {
                        ForEach(users) { us in
                            NavigationLink(destination:
                                            UserDetailView(user: us)
                            ) {
                                UserRow(user: us)
                            }
                            .id(us.id)
                        }
                        .onDelete(perform: deleteUsers(from:))
                    }
                    .listRowBackground(Color.green)
                }
            .listStyle(InsetGroupedListStyle())
            .navigationTitle("Listado de Usuarios")
            .navigationBarItems(trailing:
                HStack {
                    EditButton()
                    Button(action: {
                        self.showAddUser = true
                    }, label: {
                        Image(systemName: "plus")
                    })

                    .popover(isPresented: $showAddUser, content: {
                        UsersAddHeaderView(full: true) { newUser in
                            self.users.append(newUser)
                        }
                        .frame(minWidth:300, minHeight: 300)
                    })

                })
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
    func addStudent(student: Usuario) {
        self.users.append(student)
    }
    
    func deleteUsers(from indexSet: IndexSet) {
        for index in indexSet {
            self.users.remove(at: index)
        }
    }
    
}

struct UserListView_Previews: PreviewProvider {
    static var previews: some View {
        
        Group {
            UserListView(users: [.init(mock: true),
                                       .init(mock: true),
                                       .init(mock: true),
                                       .init(mock: true),
                                       .init(mock: true),
                                       .init(mock: true),
                                   .init(mock: true)])
            UserListView(users: [.init(mock: true),
                                       .init(mock: true),
                                       .init(mock: true),
                                       .init(mock: true),
                                       .init(mock: true),
                                       .init(mock: true),
                                       .init(mock: true)])
                .previewDevice("iPad Pro (11-inch) (3rd generation)")
        }
            }
}
